/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.ox2;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class OX2 {
    
    static String[][] table={{"1","2","3"}, { "4", "5", "6" }, { "7", "8", "9" } };
    static String currentPlayer = "O";
    static int row,col;
    static int countTurn = 0;
    

    public static void main(String[] args) {
        while(true){
            playGame();
            continueGame();
        }
        
    }

    private static void playGame() {
        printWelcome();
        while(true){
            countTurn++;
            printTable();
            printTurn();
            InputNum();
            if(isWin()){
                printTable();
                printWin();
                break;
            }
            if (isDraw()) {
                printTable();
                printDraw();
                break;
            }
            SwitchPlayer();
        }
    }
    
    private static void continueGame() {
        Scanner kb = new Scanner(System.in);
        String conState;
        System.out.print("Continue (Y/N) ? : ");
        conState = kb.next();
        table = new String[][] { { "1", "2", "3" }, { "4", "5", "6" }, { "7", "8", "9" } };
        countTurn = 0;
        if (conState.equals("N")) {
            System.out.println("Thank You For Playing the Game.");
            System.out.println("Goodbye.");
            System.exit(0);
        }

    }
    
    private static void printWelcome() {
       System.out.println("Welcome to OX Games");
       System.out.println("Table of numbers");
    }

    private static void printTable() {
        for(int i=0;i<3;i++) {
            for(int j=0;j<3;j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println("");
        }
        
        
    }

    private static void printTurn() {
        System.out.println("Turn "+currentPlayer);
    }

    private static void InputNum() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please Input Number to Place : ");
        int num = kb.nextInt();
        col = (num-1)%3;
        row = (num-1)/3;
        while (table[row][col].equals("X") || table[row][col].equals("O")) {
            System.out.println("Try Again!!!");
            System.out.print("Please Input Number to Place : ");
            num = kb.nextInt();
            col = (num - 1) % 3;
            row = (num - 1) / 3;
        }
        table[row][col] = currentPlayer;
    }

    private static void SwitchPlayer() {
        if(currentPlayer.equals("O")){
            currentPlayer = "X";
        }else{
            currentPlayer = "O";
        }
    }

    private static void printWin() {
        System.out.print(currentPlayer +" Win !!!");
    }

    private static boolean isWin() {
        for (int k = 0; k < 3; k++) {
            if (table[0][k].equals(currentPlayer) && table[1][k].equals(currentPlayer)&& table[2][k].equals(currentPlayer)) {
                return true;
            }
        }
        for (int g = 0; g < 3; g++) {
            if (table[g][0].equals(currentPlayer) && table[g][1].equals(currentPlayer)&& table[g][2].equals(currentPlayer)) {
                return true;
            }
        }
        if (table[0][0].equals(currentPlayer) && table[1][1].equals(currentPlayer)&& table[2][2].equals(currentPlayer)) {
            return true;
        }
        if (table[0][2].equals(currentPlayer) && table[1][1].equals(currentPlayer)&& table[2][0].equals(currentPlayer)) {
            return true;
        }
        return false;
    }

    private static boolean isDraw() {
        if (countTurn == 9) {
            countTurn = 0;
            return true;
        }
        return false;
    }

    private static void printDraw() {
        System.out.println("Draw!!!");
    }
}    

   

    

